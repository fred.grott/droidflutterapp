# DroidFlutterApp

![dart](dart.png) ![flutter](flutter.png)

This part of my developing two migration paths to cross platform flutter mobile 
development for startups. The basic android app is a todo app applying the 
Redux form of Flux app architecture to flux apps.

Best practices are being documenterd here in preparation of developing 
training materials.

## Flutter Wrapper

Someone came with a way to have the flutter sdk version more automated and decoupled via the same approuch as 
the gradle wrapper, see

https://github.com/passsy/flutter_wrapper

Future todo is to transition my flutter projects to use it as I do not feel like figuring out 
howe to rewrite the bash script into a windows batch bat script yet.

## AndroidStudio Project Structure

I have modified the project structure suggested by Google to have root Project Folder use a non dart 
package name format and have the subfoldedr be the dart correct package formated name. 

Project git commits happen at the ultimate project root via closing project opened at android folder and 
openingh project at project root. Limitations to this is one needs to double check all files for 
errors before the git commit as one looses the correct code analaysis warnings during the git commit 
as all android studio versions 3.3 to 3.5 use a Google Android Gradle PLugin not really setup for 
a situation where one is using a cross platform framework and only having one sub module under 
the android gradleplugin control.

## iOS

Basic ios setup but none of the boilerplate yet as do not have a MacBook Pro yet.

## CI-Server Continuous Builds

Gitlab shared runners are all linux. I will probably have to at some point configure with CircleCI as 
I understand that they might have some Mac platform shared runners under their free plan.

## AndroidGradle

Using an abbreviated best practices of android dv setup from

[DroidKt](https://gitlab.com/fred.grott/droidkt)

Since in the classic android app version we are just using an application class wrapper 
and an activity wrapper one does not need the other gradle plugins and of course 
other plugins cannot be applied anyway due to the somewhat beta condition of 
android studio gradle cross plattform mobile support.

## Fuchsia

My best guess is that Fuchsia 1.0 will be previewed by devs March of 2020 and first devices coming out in 
Dec 2020. The flutter UI-kit and rendering engine will probably be the same include mechanism where its 
included in the user space as an embedded toolkit and has a fuchsia specifc app class and activity class wrapper.
Thus, the only difference between flutter ios and android-classic wise and Fuchsi is a different system OS api 
set that get accessed via the platform channel and my best guess is that at that time will have some 
dsl helper from Google that translates from android classic system os api calls to fuchsia.

## Design Notes

The Flutter Render system and UI apis differ from the inheritence Ui system widgets of 
both iphone and classic android in that its no longer inheritence but composition. In summary 
extremely small widgets atomically doing one thing than added together to get a full layout and 
screen.

From what I am seeing compared to both default system OS UI kits of iPhone and classic Android one 
needs to take the widgets in the Material Components and Cupertine Widgets and cusotmizing and 
polish them to get to the place I want a startup to be as far as having the best iphone app and the best android 
and later the best fuchsia app.  But, all the flutter widgets are open source so I have the 
code access to learn form Google Engineers and extend what is already there in the widgets
catalog.

Note, someone did come up with a good abstract out widgets and switch from Material to Cupertino 
based on device, see

https://github.com/aqwert/flutter_platform_widgets

Despite the absence of testing code, seems to work and has pretty much no major blockers to using it.

So that reduces my design work to getting redux ap arch correct on android, ios, and later fuchsia.


# Summary

This sample android flutter todo app is an example of the free-fall method of dealing with legacy app 
code by just starting over using flutter. The other free fall approach to migrating to flutter 
involves using the mentioned flutter_platform_widgets library to  be effective at an anbstract 
common widget base and than platform specifc Cupertino and Material widgets.

The other major migrate to flutter approach as evidenced by the DdroidKt build tools project and kotlin andorid 
todo app is align reactive to use kotlin coroutines, anko for xml-less layouts and the app arch form fo flux 
called redux as intertrim step on the way to finally porting the legacy appcode base of the ios
app and the android classic app to flutter.

For those funded startups looking for Flutter Mobile Engineers who get it, my email is

fred    DOT

grott AT

gmail DOT

com



# Copyrights and Trademarks

Apple,Inc owns the ios, apple logo, etc and only referecning description of platform to describe the mobile 
cross flutter platform development. Gooogle is developing Flutter and Dart and of course owns the Android trademark and 
copyrights to the android robot icon. Any reference to terms and icons form Google is done with in reesepct to 
follow Gogole's own copyright and trademark press and media guidelines for referring to such items.


